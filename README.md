# Aplikasi Catatan Pulsa
Aplikasi android untuk menyimpan data atau catatan pulsa bagi orang konter atau yang sedang menjual pulsa.

## Basic SQLite
Project ini menggunakan database SQLite.

## Download for Android
Grab APK file [here](https://github.com/TFK-Project/catatan-pulsa/raw/master/catatan-pulsa.apk)

## Screenshot
### Password layout
![Screenshot-1](/screenshots/Screenshot_2017-09-16-01-01-58_com.tfkproject.catatan.pulsa.png?raw=true "Screenshot 1")

### Main view
![Screenshot-2](/screenshots/Screenshot_2017-09-16-01-03-40_com.tfkproject.catatan.pulsa.png?raw=true "Screenshot 2")

### Form input
![Screenshot-3](/screenshots/Screenshot_2017-09-16-01-04-04_com.tfkproject.catatan.pulsa.png?raw=true "Screenshot 3")

### Opsi konten
![Screenshot-4](/screenshots/Screenshot_2017-09-16-01-03-47_com.tfkproject.catatan.pulsa.png?raw=true "Screenshot 4")

### Form detail
![Screenshot-5](/screenshots/Screenshot_2017-09-16-01-03-53_com.tfkproject.catatan.pulsa.png?raw=true "Screenshot 5")

### Settings layout
![Screenshot-6](/screenshots/Screenshot_2017-09-16-01-03-58_com.tfkproject.catatan.pulsa.png?raw=true "Screenshot 6")

## License

[![CC0](https://www.gnu.org/graphics/gplv3-127x51.png)](https://www.gnu.org/licenses/gpl-3.0.en.html)

GNU General Public License v3.0
