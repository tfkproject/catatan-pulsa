package com.tfkproject.catatan.pulsa;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;

public class TambahData extends AppCompatActivity {

    EditText txtNm, txtNoHp;
    TextView tanggal;
    Spinner operator, jenis_pulsa, pulsa, keterangan;
    DatabaseHelper dbHelper = new DatabaseHelper(this);

    // Calender
    private Calendar cal;
    private int day, month, year;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah);
        //tampilkan menu back di action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtNm = (EditText) findViewById(R.id.txtNama);
        txtNoHp = (EditText) findViewById(R.id.txtNohp);
        operator = (Spinner) findViewById(R.id.daftar_jenis_op);
        jenis_pulsa = (Spinner) findViewById(R.id.daftar_jenis_pulsa);
        pulsa = (Spinner) findViewById(R.id.daftar_pulsa);
        tanggal = (TextView) findViewById(R.id.txtTanggal);
        keterangan = (Spinner) findViewById(R.id.daftar_ket);

        //        C A L E N D E R
        cal     = Calendar.getInstance();
        day     = cal.get(Calendar.DAY_OF_MONTH);
        month   = cal.get(Calendar.MONTH);
        year    = cal.get(Calendar.YEAR);

        tanggal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { showDialog(0); }
        });

        Button simpan = (Button) findViewById(R.id.button);
        simpan.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                if(txtNm.getText().toString().length() > 0 && txtNoHp.getText().toString().length() > 0){
                    tambah_data();
                }else{
                    Toast.makeText(getApplicationContext(), "Data tidak boleh kosong", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    @Deprecated
    protected Dialog onCreateDialog(int id) {
        return new DatePickerDialog(this, datePickerListener, year, month, day);
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            NumberFormat f = new DecimalFormat("00");
            tanggal.setText(f.format(selectedDay) + "-" + (f.format(selectedMonth + 1)) + "-" + selectedYear );
        }
    };

    private void tambah_data(){
        try{
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            String nm = txtNm.getText().toString();
            String no_hp = txtNoHp.getText().toString();
            String op = String.valueOf(operator.getSelectedItem());
            String j_pls = String.valueOf(jenis_pulsa.getSelectedItem());
            String pls = String.valueOf(pulsa.getSelectedItem());
            String tgl = tanggal.getText().toString();
            String ket = String.valueOf(keterangan.getSelectedItem());

            String insert_data = "INSERT INTO tbl_catatan (id_catatan, nama, no_hp, operator, jenis_pulsa, pulsa, tanggal, ket)" +
                    "VALUES (NULL, '"+nm+"', '"+no_hp+"', '"+op+"', '"+j_pls+"', '"+pls+"', '"+tgl+"', '"+ket+"')";
            db.execSQL(insert_data);

            //Toast.makeText(getApplicationContext(), "Data berhasil disimpan", Toast.LENGTH_LONG).show();

            TambahData.this.finish();

        }catch (SQLiteException er){
            er.getStackTrace();
            Toast.makeText(getApplicationContext(), "Data gagal disimpan, terjadi kesalahan", Toast.LENGTH_LONG).show();

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {
            case android.R.id.home:
                // menutup activity ini
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
