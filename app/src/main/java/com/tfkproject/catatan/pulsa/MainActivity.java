package com.tfkproject.catatan.pulsa;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.util.ArrayList;
import java.util.HashMap;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfWriter;

public class MainActivity extends AppCompatActivity {

    //String[] daftar, daftar_id;
    ListView lv;
    protected Cursor c;
    DatabaseHelper dbcenter = new DatabaseHelper(this);
    SwipyRefreshLayout mSwipy;
    ListAdapter simpleAdapter;
    ArrayList<HashMap<String, String>> daftar = new ArrayList<HashMap<String, String>>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(MainActivity.this, TambahData.class);
                startActivity(in);
            }
        });

        mSwipy = (SwipyRefreshLayout) findViewById(R.id.swipyrefreshlayout);
        mSwipy.setDirection(SwipyRefreshLayoutDirection.TOP);
        mSwipy.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                RefreshList();

                //refresh handler
                mSwipy.setRefreshing(true);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //sembunyikan refresh loading setelah 2 detik
                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mSwipy.setRefreshing(false);
                            }
                        });
                    }
                }, 1000);
            }
        });

        RefreshList();
    }

    public void RefreshList(){
        //bersihkan dulu list
        daftar.clear();

        SQLiteDatabase db = dbcenter.getReadableDatabase();
        c = db.rawQuery("SELECT * FROM tbl_catatan order by id_catatan DESC",null);

        String[] daftar_id = new String[c.getCount()];
        String[] daftar_nama = new String[c.getCount()];
        String[] daftar_pulsa = new String[c.getCount()];
        String[] daftar_tgl = new String[c.getCount()];
        String[] daftar_ket = new String[c.getCount()];

        c.moveToFirst();
        for (int cc=0; cc < c.getCount(); cc++){
            c.moveToPosition(cc);

            daftar_id[cc] = c.getString(c.getColumnIndex("id_catatan"));
            daftar_nama[cc] = c.getString(c.getColumnIndex("nama"));
            daftar_pulsa[cc] = c.getString(c.getColumnIndex("pulsa"));
            daftar_tgl[cc] = c.getString(c.getColumnIndex("tanggal"));
            daftar_ket[cc] = c.getString(c.getColumnIndex("ket"));

            HashMap<String, String> map = new HashMap<String, String>();
            map.put("key_id", daftar_id[cc]);
            map.put("key_nama", daftar_nama[cc]);
            //map.put("key_pulsa", "Pulsa: Rp. "+daftar_pulsa[cc]);
            map.put("key_tanggal", daftar_tgl[cc]);
            map.put("key_ket", daftar_ket[cc]);

            daftar.add(map);
        }

        lv = (ListView)findViewById(R.id.listView1);

        simpleAdapter = new SimpleAdapter(this, daftar, R.layout.list_item,
                new String[] {
                        "key_id",
                        "key_nama",
                        "key_tanggal",
                        "key_ket"
                },
                new int[] {
                        R.id.text_id,
                        R.id.text_nama,
                        R.id.text_tanggal,
                        R.id.text_ket
                });

        //lv.setAdapter(new ArrayAdapter(this, android.R.layout.simple_list_item_1, daftar));
        lv.setAdapter(simpleAdapter);

        lv.setSelected(true);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView arg0, View arg1, int arg2, long arg3) {
                //final String selection = daftar_id[arg2]; //.getItemAtPosition(arg2).toString();
                final String id = ((TextView) arg1.findViewById(R.id.text_id)).getText().toString();
                final CharSequence[] dialogitem = {"Lihat", "Hapus"};
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Pilihan");
                builder.setItems(dialogitem, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        switch(item){
                            /*case 0 :
                                Intent i = new Intent(getApplicationContext(), LihatAkun.class);
                                i.putExtra("id_akn", id);
                                startActivity(i);
                                break;*/
                            case 0 :
                                Intent in = new Intent(getApplicationContext(), UpdateData.class);
                                in.putExtra("id_ctn", id);
                                startActivity(in);
                                break;
                            case 1 :
                                SQLiteDatabase db = dbcenter.getWritableDatabase();
                                String q = "delete from tbl_catatan where id_catatan = '"+id+"'";
                                db.execSQL(q);
                                RefreshList();
                                break;
                        }
                    }
                });
                builder.create().show();
            }});
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(getApplicationContext(), SettingsActivity.class);
            startActivity(i);

            return true;
        }
        if (id == R.id.action_export) {
            //cek permission di android M
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                checkPermission();
            }

            //task here
            exportToPDF();

            return true;
        }
        if (id == R.id.action_about){
            //tentang aplikasi
            AlertDialog.Builder bl = new AlertDialog.Builder(this);
                    bl.setTitle("Catatan Pulsa");
                    bl.setMessage("Versi 1.0.0\nDidevelop oleh @t4ufik_hidayat");
                    /*bl.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                        }
                    });*/
                    bl.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    });
                    bl.setIcon(R.mipmap.ic_launcher);
                    bl.show();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void exportToPDF() {
        Document doc = new Document();

        try {
            String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Catatan Pulsa";

            File dir = new File(path);
            if(!dir.exists())
                dir.mkdirs();

            Log.d("PDFCreator", "PDF Path: " + path);

            File file = new File(dir, "catatan_pulsa.pdf");
            FileOutputStream fOut = new FileOutputStream(file);

            PdfWriter.getInstance(doc, fOut);

            //open the document
            doc.open();

			/* Create Paragraph and Set Font */
            Paragraph p1 = new Paragraph("Data catatan pulsa");
			/* Create Set Font and its Size */
            Font paraFont= new Font(Font.HELVETICA);
            paraFont.setSize(20);
            p1.setAlignment(Paragraph.ALIGN_CENTER);
            p1.setFont(paraFont);

            //add paragraph to document
            doc.add(p1);


            //Paragraph p2 = new Paragraph("This is an example of a simple paragraph");
			/* You can also SET FONT and SIZE like this */
            //Font paraFont2= new Font(Font.COURIER,14.0f, Color.GREEN);
            //p2.setAlignment(Paragraph.ALIGN_CENTER);
            //p2.setFont(paraFont2);

            //doc.add(p2);

			/* Inserting Image in PDF */
            /*ByteArrayOutputStream stream = new ByteArrayOutputStream();
            Bitmap bitmap = BitmapFactory.decodeResource(getBaseContext().getResources(), R.mipmap.ic_launcher);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100 , stream);
            Image myImg = Image.getInstance(stream.toByteArray());
            myImg.setAlignment(Image.MIDDLE);

            //add image to document
            doc.add(myImg);*/

            /*Data catatan*/
            Paragraph p2 = new Paragraph("| NAMA | PULSA | TANGGAL | KETERANGAN |");
			/* You can also SET FONT and SIZE like this */
            Font paraFont2= new Font(Font.COURIER,14.0f, Color.GREEN);
            p2.setAlignment(Paragraph.ALIGN_CENTER);
            p2.setFont(paraFont2);
            doc.add(p2);

            SQLiteDatabase db = dbcenter.getReadableDatabase();
            c = db.rawQuery("SELECT * FROM tbl_catatan order by id_catatan DESC",null);

            String[] daftar_id = new String[c.getCount()];
            String[] daftar_nama = new String[c.getCount()];
            String[] daftar_pulsa = new String[c.getCount()];
            String[] daftar_tgl = new String[c.getCount()];
            String[] daftar_ket = new String[c.getCount()];

            c.moveToFirst();
            for (int cc=0; cc < c.getCount(); cc++){
                c.moveToPosition(cc);

                daftar_id[cc] = c.getString(c.getColumnIndex("id_catatan"));
                daftar_nama[cc] = c.getString(c.getColumnIndex("nama"));
                daftar_pulsa[cc] = c.getString(c.getColumnIndex("pulsa"));
                daftar_tgl[cc] = c.getString(c.getColumnIndex("tanggal"));
                daftar_ket[cc] = c.getString(c.getColumnIndex("ket"));

                Paragraph p3 = new Paragraph("| "+daftar_nama[cc]+" | "+daftar_pulsa[cc]+" | "+daftar_tgl[cc]+" | "+daftar_ket[cc]+" |");
			    /* You can also SET FONT and SIZE like this */
                Font paraFont3= new Font(Font.COURIER,14.0f, Color.GREEN);
                p3.setAlignment(Paragraph.ALIGN_CENTER);
                p3.setFont(paraFont3);
                doc.add(p3);
            }


            //set footer
            Phrase footerText = new Phrase("Generated by Catatan Pulsa");
            HeaderFooter pdfFooter = new HeaderFooter(footerText, false);
            doc.setFooter(pdfFooter);

            Toast.makeText(getApplicationContext(), "File berhasil di export", Toast.LENGTH_LONG).show();

        } catch (DocumentException de) {
            Log.e("PDFCreator", "DocumentException:" + de);
        } catch (IOException e) {
            Log.e("PDFCreator", "ioException:" + e);
        }
        finally
        {
            doc.close();

            //open PDF
            openPdf();
        }
    }

    void openPdf()
    {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Catatan Pulsa";

        File file = new File(path, "catatan_pulsa.pdf");

        try{
            intent.setDataAndType( Uri.fromFile( file ), "application/pdf" );
            startActivity(intent);
        }
        catch (ActivityNotFoundException e){
            Toast.makeText(MainActivity.this, "Tidak ditemukan aplikasi untuk membuka file PDF", Toast.LENGTH_SHORT).show();
        }
    }

    public static final int MY_PERMISSIONS_REQUEST = 99;
    public boolean checkPermission(){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {

                        //DO TASK HERE
                        exportToPDF(); //export to PDF
                    }

                } else {

                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        RefreshList();
    }
}
