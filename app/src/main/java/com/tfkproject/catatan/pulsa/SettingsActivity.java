package com.tfkproject.catatan.pulsa;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SettingsActivity extends AppCompatActivity {

    EditText edt,edt1;
    Button btnSimpan, btnRst;

    protected Cursor c;
    DatabaseHelper dbcenter_ = new DatabaseHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        //tampilkan menu back di action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        edt = (EditText) findViewById(R.id.editText);
        edt1 = (EditText) findViewById(R.id.editText1);

        btnSimpan = (Button) findViewById(R.id.btnSet);
        btnSimpan.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                if(edt.getText().toString().length() > 0 && edt1.getText().toString().length() > 0){
                    SQLiteDatabase db = dbcenter_.getWritableDatabase();
                    c = db.rawQuery("SELECT * FROM tbl_admin WHERE id_admin = '1';",null);
                    c.moveToFirst();
                    if (c.getCount()>0)
                    {
                        c.moveToPosition(0);
                        String pass_old = c.getString(c.getColumnIndex("password"));
                        if(edt.getText().toString().contains(pass_old)){
                            //Toast.makeText(getApplicationContext(), "Password sebelumnya sama", Toast.LENGTH_LONG).show();
                            update_pass();
                        }else{
                            Toast.makeText(getApplicationContext(), "Password sebelumnya tidak cocok", Toast.LENGTH_LONG).show();
                        }
                    }
                }else{
                    Toast.makeText(getApplicationContext(), "Tidak boleh kosong", Toast.LENGTH_LONG).show();
                }
            }
        });

        btnRst = (Button) findViewById(R.id.btnReset);
        btnRst.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                reset_pass();
            }
        });
    }

    private void update_pass(){
        try{
            SQLiteDatabase db = dbcenter_.getWritableDatabase();


            String pass = edt1.getText().toString();

            String insert_jenis = "UPDATE tbl_admin SET password = '"+pass+"' WHERE id_admin = '1';";
            db.execSQL(insert_jenis);

            Toast.makeText(getApplicationContext(), "Password berhasil diupdate", Toast.LENGTH_LONG).show();

            SettingsActivity.this.finish();

        }catch (SQLiteException er){
            er.getStackTrace();
            Toast.makeText(getApplicationContext(), "Gagal mengganti password, terjadi kesalahan", Toast.LENGTH_LONG).show();

        }
    }

    private void reset_pass(){
        try{
            SQLiteDatabase db = dbcenter_.getWritableDatabase();

            String insert_jenis = "UPDATE tbl_admin SET password = '123456' WHERE id_admin = '1';";
            db.execSQL(insert_jenis);

            Toast.makeText(getApplicationContext(), "Password berhasil direset ke default", Toast.LENGTH_LONG).show();
        }catch (SQLiteException er){
            er.getStackTrace();
            Toast.makeText(getApplicationContext(), "Gagal, terjadi kesalahan", Toast.LENGTH_LONG).show();

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {
            case android.R.id.home:
                // menutup activity ini
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
