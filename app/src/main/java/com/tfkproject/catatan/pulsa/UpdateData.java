package com.tfkproject.catatan.pulsa;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;

public class UpdateData extends AppCompatActivity {

    EditText txtNm, txtNohp, txtKet;
    TextView txtOp, txtJPulsa, txtPlsa, txtTanggal;
    protected Cursor c;
    DatabaseHelper dbcenter_ = new DatabaseHelper(this);
    // Calender
    private Calendar cal;
    private int day, month, year;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        //tampilkan menu back di action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtNm = (EditText) findViewById(R.id.txtNama);
        txtNohp = (EditText) findViewById(R.id.txtNohp);
        txtOp = (TextView) findViewById(R.id.txtOp);
        txtJPulsa = (TextView) findViewById(R.id.txtJenisPulsa);
        txtPlsa = (TextView) findViewById(R.id.txtPlsa);
        txtTanggal = (TextView) findViewById(R.id.txtTanggal);
        txtKet = (EditText) findViewById(R.id.txtKet);

        //        C A L E N D E R
        cal     = Calendar.getInstance();
        day     = cal.get(Calendar.DAY_OF_MONTH);
        month   = cal.get(Calendar.MONTH);
        year    = cal.get(Calendar.YEAR);

        txtTanggal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { showDialog(0); }
        });

        SQLiteDatabase db = dbcenter_.getWritableDatabase();

        String id = getIntent().getExtras().getString("id_ctn");
        c = db.rawQuery("SELECT * FROM tbl_catatan WHERE id_catatan = '" +id+ "';",null);
        c.moveToFirst();
        if (c.getCount()>0)
        {
            c.moveToPosition(0);
            String nm = c.getString(c.getColumnIndex("nama"));
            getSupportActionBar().setTitle("Data "+nm);

            String nama = c.getString(c.getColumnIndex("nama"));
            txtNm.setText(nama);
            String no_hp = c.getString(c.getColumnIndex("no_hp"));
            txtNohp.setText(no_hp);
            String opt = c.getString(c.getColumnIndex("operator"));
            txtOp.setText(opt);
            String jpls = c.getString(c.getColumnIndex("jenis_pulsa"));
            txtJPulsa.setText(jpls);
            String pls = c.getString(c.getColumnIndex("pulsa"));
            txtPlsa.setText("Rp. "+pls);
            String tgl = c.getString(c.getColumnIndex("tanggal"));
            txtTanggal.setText(tgl);
            String ket = c.getString(c.getColumnIndex("ket"));
            txtKet.setText(ket);
        }

        Button simpan = (Button) findViewById(R.id.button);
        simpan.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                if(txtNm.getText().toString().length() > 0 && txtNohp.getText().toString().length() > 0 && txtKet.getText().toString().length() > 0){
                    update_akun();
                }else{
                    Toast.makeText(getApplicationContext(), "Data tidak boleh kosong", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    @Deprecated
    protected Dialog onCreateDialog(int id) {
        return new DatePickerDialog(this, datePickerListener, year, month, day);
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            NumberFormat f = new DecimalFormat("00");
            txtTanggal.setText(f.format(selectedDay) + "-" + (f.format(selectedMonth + 1)) + "-" + selectedYear );
        }
    };

    private void update_akun(){
        try{
            SQLiteDatabase db = dbcenter_.getWritableDatabase();

            String id = getIntent().getExtras().getString("id_ctn");
            String nm = txtNm.getText().toString();
            String hp = txtNohp.getText().toString();
            String tgl = txtTanggal.getText().toString();
            String ket = txtKet.getText().toString();

            String query = "UPDATE tbl_catatan SET nama = '"+nm+"', no_hp = '"+hp+"', tanggal = '"+tgl+"', ket = '"+ket+"' WHERE id_catatan = '" +id+ "';";
            db.execSQL(query);

            //Toast.makeText(getApplicationContext(), "Data berhasil update", Toast.LENGTH_LONG).show();

            UpdateData.this.finish();

        }catch (SQLiteException er){
            er.getStackTrace();
            Toast.makeText(getApplicationContext(), "Data gagal update, terjadi kesalahan", Toast.LENGTH_LONG).show();

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {
            case android.R.id.home:
                // menutup activity ini
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
