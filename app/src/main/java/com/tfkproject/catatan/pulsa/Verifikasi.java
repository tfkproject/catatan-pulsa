package com.tfkproject.catatan.pulsa;

import android.content.Intent;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Verifikasi extends AppCompatActivity {

    EditText txtPass;
    Button vr;
    protected Cursor c;
    DatabaseHelper dbcenter = new DatabaseHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verifikasi);

        txtPass = (EditText) findViewById(R.id.txtVPass);

        vr = (Button) findViewById(R.id.btnVr);
        vr.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                //verifikasi password
                verifikasi();
            }
        });
    }

    private void verifikasi(){
        try{
            if (txtPass.getText().toString().length() > 0){
                String pass = txtPass.getText().toString();
                SQLiteDatabase db = dbcenter.getReadableDatabase();
                c = db.rawQuery("SELECT * FROM tbl_admin where username = 'master' and password = '"+pass+"' ",null);
                //move cursor to first position
                c.moveToFirst();
                //fetch all data one by one
                do
                {
                    //we can use c.getString(0) here
                    //or we can get data using column index
                    String username = c.getString(c.getColumnIndex("username"));
                    String password = c.getString(c.getColumnIndex("password"));

                    Intent in = new Intent(Verifikasi.this, MainActivity.class);
                    startActivity(in);

                    Verifikasi.this.finish();

                }while(c.moveToNext());
            }else{
                Toast.makeText(getApplicationContext(), "Password kosong", Toast.LENGTH_LONG).show();
            }
        }catch (CursorIndexOutOfBoundsException e){
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Maaf, password salah!", Toast.LENGTH_LONG).show();
        }
    }

}
