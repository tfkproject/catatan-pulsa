package com.tfkproject.catatan.pulsa;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by taufik on 10/29/16.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "catatanpulsa.db";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        //tabel Admin
        String CREATE_TBL_ADM = "CREATE TABLE tbl_admin ("
                + " id_admin INTEGER PRIMARY KEY,"
                + " username TEXT,"
                + " password TEXT" + ")";
        db.execSQL(CREATE_TBL_ADM);

        //tabel Akun
        String CREATE_TBL_CTT = "CREATE TABLE tbl_catatan ("
                + " id_catatan INTEGER PRIMARY KEY AUTOINCREMENT,"
                + " nama TEXT,"
                + " no_hp TEXT,"
                + " operator TEXT,"
                + " jenis_pulsa TEXT,"
                + " pulsa TEXT,"
                + " tanggal TEXT,"
                + " ket TEXT" + ")";
        db.execSQL(CREATE_TBL_CTT);

        //insert ke tabel Admin

        String insert_admin = "INSERT INTO tbl_admin (id_admin, username, password)" +
                "VALUES ('1', 'master', '123456')";
        db.execSQL(insert_admin);

    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS tbl_admin");
        // Create tables again
        onCreate(db);
    }
}

